package com.snakefish.utils;

public final class ArrayUtils {

    private ArrayUtils() {
    }

    public static boolean isEmpty(Object[] array) {
        return array == null || array.length == 0;
    }

    public static boolean isNotEmpty(Object[] array) {
        return !isEmpty(array);
    }

    public static int itemsCount(Object[][] array) {
        int itemsCount = 0;
        for (int i = 0; i < array.length; i++) {
            itemsCount += array[i].length;
        }
        return itemsCount;
    }

    public static Object findItemByLinearPosition(Object[][] array, int linearPosition) {
        for (int i = 0; i < array.length; i++) {
            int innerArrayLength = array[i].length;
            if (linearPosition >= innerArrayLength) {
                linearPosition -= innerArrayLength;
            } else {
                return array[i][linearPosition];
            }
        }
        return null;
    }

}
