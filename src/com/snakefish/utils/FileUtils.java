package com.snakefish.utils;

import java.io.Closeable;
import java.io.IOException;

public final class FileUtils {

    private FileUtils() {
    }

    public static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException ex) {
            }
        }
    }

}
