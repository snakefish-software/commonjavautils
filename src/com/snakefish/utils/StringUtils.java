package com.snakefish.utils;

public final class StringUtils {

    private StringUtils() {
    }

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static boolean isEmptyOrWhitespace(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static boolean isNotEmptyOrWhitespace(String str) {
        return !isEmptyOrWhitespace(str);
    }

    public static boolean atLeastOneIsEmpty(String... strs) {
        for (String str : strs) {
            if (isEmpty(str)) {
                return true;
            }
        }
        return false;
    }

    public static boolean atLeastOneIsEmptyOrWhitespace(String... strs) {
        for (String str : strs) {
            if (isEmptyOrWhitespace(str)) {
                return true;
            }
        }
        return false;
    }

}
