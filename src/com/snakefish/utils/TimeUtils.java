package com.snakefish.utils;

public final class TimeUtils {

    private TimeUtils() {
    }

    public static String formatTime(long millis) {
        long timeInSeconds = millis / 1000;

        long minutesPart = timeInSeconds / 60;
        long secondsPart = timeInSeconds % 60;

        return minutesPart + ":" + formatTimePart(secondsPart);
    }

    private static String formatTimePart(long timePart) {
        return timePart > 9 ? String.valueOf(timePart) : "0" + timePart;
    }

}
