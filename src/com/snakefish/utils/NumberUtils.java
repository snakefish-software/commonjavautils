package com.snakefish.utils;

import java.text.DecimalFormat;
import java.text.ParseException;

public final class NumberUtils {

    private static final double EPSILON = 0.0000001;
    private static final DecimalFormat decimalFormat = new DecimalFormat("#.##");
    public static final int DEFAULT_PRECISION = decimalFormat.getMaximumFractionDigits();

    private NumberUtils() {
    }

    public static boolean isNumber(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public static boolean isNotNumber(String str) {
        return !isNumber(str);
    }

    public static boolean atLeastOneIsNotNumber(String... strs) {
        for (String str : strs) {
            if (isNotNumber(str)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isInt(double d) {
        return Math.abs(d - (long) d) < EPSILON;
    }

    public static boolean isNotInt(double d) {
        return !isInt(d);
    }

    public static boolean isInt(double d, int i) {
        return Math.abs(d - i) < EPSILON;
    }

    public static boolean isNotInt(double d, int i) {
        return !isInt(d, i);
    }

    public static boolean doubleHasPrecision(double val, int precision) {
        String valStr = toString(val, precision);
        double valToCompare = fromString(valStr, precision);
        return Math.abs(val - valToCompare) < EPSILON;
    }

    private static double fromString(String valStr, int maxPrecision) {
        int oldMaxPresision = decimalFormat.getMaximumFractionDigits();
        decimalFormat.setMaximumFractionDigits(maxPrecision);
        try {
            return decimalFormat.parse(valStr).doubleValue();
        } catch (ParseException e) {
            return Double.parseDouble(valStr);
        } finally {
            decimalFormat.setMaximumFractionDigits(oldMaxPresision);
        }
    }

    public static String toString(double val) {
        return toString(val, false);
    }

    public static String toString(double val, int maxPrecision) {
        return toString(val, maxPrecision, false);
    }

    public static String toString(double val, boolean addParenthesisIfNegative) {
        return toString(val, decimalFormat.getMaximumFractionDigits(), addParenthesisIfNegative);
    }

    public static String toString(double val, int maxPrecision, boolean addParenthesisIfNegative) {
        int oldMaxPresision = decimalFormat.getMaximumFractionDigits();
        decimalFormat.setMaximumFractionDigits(maxPrecision);
        String strRepr = decimalFormat.format(val);
        decimalFormat.setMaximumFractionDigits(oldMaxPresision);

        if (val < 0 && addParenthesisIfNegative) {
            strRepr = "(" + strRepr + ")";
        }

        return strRepr;
    }

}
